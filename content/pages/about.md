+++
title = "About"
author = ["CarlosPerez"]
draft = false
[menu.about]
  weight = 2001
  identifier = "about"
+++

My name is The Dude. I have the following qualities:

-   I rock a great beard
-   I'm extremely loyal to my friends
-   I like bowling

That rug really tied the room together.